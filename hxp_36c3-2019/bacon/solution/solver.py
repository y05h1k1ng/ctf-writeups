from ptrlib import *
import subprocess
import binascii

while True:
    sock = Socket('localhost', 8000)
    h = sock.recvline().strip()
    print('[+] bruteforcing:', h)
    popen = subprocess.Popen(["./meet", h], stdout=subprocess.PIPE)
    output = popen.stdout.read().strip()
    if output != b"":
        print("[+] H({}) = {}".format(output, h))
        sock.sendline(output)
        print(sock.recvline())
        exit()
    sock.close()
