# CTF Writeups

***ただの復習記録なので、詳しい解説や内容の正確性はないです***

各問題のREADME.mdやwriteup.ipynbとかにwriteupを書いています
(Cryptoとか数式を使うものはjupyter notebookを使いがち)

gitlab上でjupyter notebookを表示させる方法があるのでググってください

| CTF | Challenge | Tags | Difficulty |
|----|-----|----|----|
| Balsn CTF 2019 | Collision | crypto, crc, hash | hard |
| Hitcon CTF 2019 | Lost Modulus Again | crypto, rsa | medium |
| GreHack CTF 2019 | shady secrets sharing | crypto | easy |
| ASIS Final CTF 2019 | True zero | crypto, rev | hard |
| hxp 36c3 CTF 2019 | bacon | crypto, block-cipher, hash | medium |
| Plaid CTF 2015 | lazy | crypto, lll | medium |
