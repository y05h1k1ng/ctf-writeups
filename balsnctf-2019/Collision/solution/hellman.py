#-*- coding:utf-8 -*-

from __future__ import print_function, division

from sage.all import *

import subprocess, json, os
from subprocess import PIPE

if not os.path.exists("cache"):
    os.mkdir("cache")

def tobin(x, n):
    return tuple(ZZ(x).digits(2, padto=n)[::-1])

def frombin(v):
    return int("".join(map(str, v)), 2)

def omnihash(x):
    """All affine funcs concatenated"""
    # cache the result for reruns
    f = "cache/%s" % x.encode("hex")
    if not os.path.exists(f):
        res = subprocess.Popen(['omnihash', '-jcs'], stdin=PIPE, stdout=PIPE)
        res, _ = res.communicate(x)
        with open(f, "w") as fd:
            fd.write(res)
    return parse(open(f).read())

def parse(res):
    res = json.loads(res)
    while not isinstance(res, dict):
        res = res[0]
    h = ""
    for k in sorted(res):
        if k.startswith("CRC") or \
           k in "X-25 XMODEM MODBUS POSIX JAMCRC KERMIT XFER":
            # assume 64-bit value by default
            # just to reduce dimension,
            # we can hint the actual size
            t = res[k].lstrip("0x").rstrip("L").zfill(16)
            if "-8" in k: t = t[-2:]
            if "-16" in k: t = t[-4:]
            if "-24" in k: t = t[-6:]
            if "-32" in k: t = t[-8:]
            if "CIT" in k: t = t[-4:]
            h += t
    return h

# 75 = size(p)
# 91 matches the omnihash Length field
SHA256_HASH = "821d0319b4d80a89af03ff7df5bb7aac56363c38000273acb7e6a2858d3267b5"
FLAG_SHA1 = "3a4a6c4047f9350493cdabcf719d8e4f3b3c1f2f"
MSG_BYTES = 75 + 8 + 8
MSG_BITS = MSG_BYTES * 8
HASHLEN = len(omnihash("qwe")) * 4

def hash2vec(h):
    return vector(GF(2), tobin(int(h, 16), HASHLEN))

def vec2str(vec):
    assert len(vec) % 8 == 0
    s = [chr(frombin(vec[i:i+8])) for i in xrange(0, len(vec), 8)]
    return "".join(s)

def str2vec(s):
    v = ()
    for c in s:
        v += tobin(ord(c), 8)
    return vector(GF(2), v)

def hashvec(vec):
    assert len(vec) == MSG_BITS
    return hash2vec(omnihash(vec2str(vec)))


# iterate DES 10 times on QAQQAQQQ
prefix = "5a208546900315b4".decode("hex")
suffix = "OAOOAOOO"

target_hash = parse(open("hash.json").read())

eqs = []

# the constant part
c0 = hashvec([0] * MSG_BITS)


target = hash2vec(target_hash)
# remove constant part
target += c0
# account suffix
target += hashvec([0] * ((MSG_BYTES - 8) * 8) + list(str2vec(suffix))) + c0
# account prefix
target += hashvec(list(str2vec(prefix)) + [0] * ((MSG_BYTES - 8) * 8)) + c0

# use the black-box oracle to recover the matrix
for i in xrange(len(prefix), MSG_BYTES-len(suffix)):
    for j in xrange(8):
        s = [0] * MSG_BITS
        s[i*8 + j] = 1
        eqs.append(hashvec(s))
    print("byte", i, "done")

mat = matrix(GF(2), eqs).transpose()
print(mat.nrows(), "x", mat.ncols(), ":", mat.rank())
print("kernel dimension:", mat.ncols() - mat.rank())

import hashlib
base = mat.solve_right(target)
# iterate over all solutions
# get the right one by com
for z in mat.right_kernel():
    s = prefix + vec2str(base + z) + suffix
    h = hashlib.sha256(s).hexdigest()
    if h == SHA256_HASH:
        assert omnihash(s) == target_hash
        print("got THE solution!")
        print(repr(s))
        break
else:
    quit(print("Fail"))

# use sage -sh to install pycrypto
from Crypto.Cipher import AES, DES
from Crypto.Util.number import bytes_to_long, long_to_bytes

# invert DES transformations
s = s[8:-8]
k = 'QAQQAQQQ'
ks = []
for _ in range(10):
    k = hashlib.sha256(k[:8]).digest()[:8]
    ks.append(k)
for k in ks[::-1]:
    des = DES.new(k, DES.MODE_CFB, k)
    s = des.decrypt(s)

print(repr(s))

# invert AES transformations
for _ in range(10):
    k = s[-16:]
    s = s[:-16:]
    aes = AES.new(k, AES.MODE_CFB, k)
    s = aes.decrypt(s)
    s = k + s

print(repr(s))

p = 0x6816b2bba5ad70478c1beadb176b9ab17cb172841b10277f538f9d837f22bdd807b970605c63859c739571cc535fd0c6879149b2d2eb676a182fd75ff343e75a22ce75c36a775157c34f17
d = int(inverse_mod(31337, p-1))
s = bytes_to_long(s)
s = pow(s, d, p)
s = long_to_bytes(s)
print("Password hex:", s.encode("hex"), repr(s), sep="\n")
assert hashlib.sha1(s).hexdigest() == FLAG_SHA1

open("password", "w").write(s.encode("hex") + "\n")
os.system("cat password | nc collision.balsnctf.com 5451")
