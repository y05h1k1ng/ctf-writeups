# from ducnt import <3. GLHF everyone

from flask import Flask, render_template, json, request, redirect, session, jsonify, url_for
from flaskext.mysql import MySQL
from werkzeug import generate_password_hash, check_password_hash
import os
import uuid
import json
import time
import logging
import hashlib
import string
import pycurl
import pylibmc
import cgi

from pymemcache.client import base
from io import BytesIO
from contextlib import closing
from flask_session import Session


mysql = MySQL()
app = Flask(__name__)
app.secret_key = '###CENSORED###'


app.config['MYSQL_DATABASE_USER'] = '###CENSORED###'
app.config['MYSQL_DATABASE_PASSWORD'] = '###CENSORED###'

app.config['MYSQL_DATABASE_DB'] = 'hellovietnamv2'
app.config['MYSQL_DATABASE_HOST'] = "There's no place like home dude!!!"
app.config['UPLOAD_FOLDER'] = 'static/Uploads'
app.config["ALLOWED_IMAGE_EXTENSIONS"] = [".jpg",".jpeg",".png",".gif"]
app.config["ALLOWED_VIDEO_EXTENSIONS"] = [".mp4",".avi"]
app.config['MAX_CONTENT_LENGTH'] = 2 * 1024 * 1024
app.config["WAFWTF"] = ["../","union","select","from","where","ftp","ssh","fastcgi","redis","zabbix","mysql","smtp","file","mail","curl"]
app.config["STRING_ONLY"] = string.ascii_letters
app.config["SECRET_KEY"] = "###CENSORED###"

upload_secret_key = "###CENSORED###"

mysql.init_app(app)


@app.route('/')
def main():
    if session.get('user'):
        return render_template('dashboard.html')
    else:
        return render_template('index.html')

@app.route('/showAddBlog', methods=['GET', 'POST'])
def showAddBlog():
    if session.get('user'):
        return render_template('addBlog.html')
    else:
        return render_template('index.html')

@app.route('/showDashboard')
def showDashboard():
    if session.get('user'):
        return render_template('dashboard.html')
    else:
        return render_template('signin.html')

@app.route('/showSignin')
def showSignin():
    if session.get('user'):
        return render_template('dashboard.html')
    else:
        return render_template('signin.html')


@app.route('/showSignUp')
def showSignUp():
    return render_template('signup.html')


@app.route('/logout')
def logout():
    session.pop('user',None)
    return redirect('/')

@app.route('/getflag')
def _getflag():
    return render_template('getflag.html')

@app.route('/showGIFmaker', methods=['GET', 'POST'])
def showGIFmaker():
    if session.get('user'):
        return render_template('GIFmaker.html')
    else:
        return render_template('index.html')

def csrf_handmade(_timestamp):
    _time_is_money = int(time.time())
    if 0 < _time_is_money - int(_timestamp) < 20:
        return True
    else:
        return False

def parse(_url):
    _obj = BytesIO()
    crl = pycurl.Curl()
    crl.setopt(crl.URL, _url)
    crl.setopt(crl.WRITEDATA, _obj)
    crl.setopt(pycurl.TIMEOUT, 60)
    crl.perform()
    crl.close()
    _data = _obj.getvalue()
    return _data.encode('hex')
    
#Only 8 lines of code will speed-up HelloVietNamv2 more 0x101 than previous version xD
def racingboiz101(_key,_value):
    _speedup101 = pylibmc.Client(["127.0.0.1:11211"], binary=False)
    if _speedup101.get(_key) == None:
        _speedup101.set(_key,"Copyright@HelloVietNamv2",time=60)
        _speedup101.set(_value,_value,time=60)
        return _speedup101.get(_key),_speedup101.get(_value)
    else:
        _speedup101.set(_value,_value,time=60)
        return _speedup101.get(_key),_speedup101.get(_value)

@app.route('/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        file = request.files['file']
        _name =  os.path.splitext(file.filename)[0]
        extension = os.path.splitext(file.filename)[1]
        extension = extension.lower()
        _time = int(time.time())
        time.sleep(0.5)
        if extension in app.config["ALLOWED_IMAGE_EXTENSIONS"]:
            f_name = hashlib.md5(upload_secret_key+_name+str(_time)).hexdigest() + extension
            if os.path.isfile(app.config['UPLOAD_FOLDER']+"/"+f_name):
                return render_template('error.html', error = 'file exist homie, try another file')
            else:
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], f_name))
                return json.dumps({'filename':f_name})
        else:
            return render_template('error.html', error = 'not today homie :D')


@app.route('/uploadvideo', methods=['GET', 'POST'])
def uploadvideo():
    if request.method == 'POST':
        file = request.files['file']
        _name =  os.path.splitext(file.filename)[0]
        extension = os.path.splitext(file.filename)[1]
        extension = extension.lower()
        _time = int(time.time())
        time.sleep(0.5)
        if extension in app.config["ALLOWED_VIDEO_EXTENSIONS"]:
            f_name = hashlib.md5(upload_secret_key+_name+str(_time)).hexdigest() + extension
            if os.path.isfile(app.config['UPLOAD_FOLDER']+"/"+f_name):
                return render_template('error.html', error = 'file exist homie, try another file')
            else:
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], f_name))
                return json.dumps({'filename':f_name})

        else:
            return render_template('error.html', error = 'not today homie :D')


@app.route('/loadexternalvideo', methods=['GET', 'POST'])
def loadexternalvideo():

    if session.get('user'):
        if request.method == 'POST':
            _title = request.form['inputTitle']
            _description = request.form['inputDescription']
            _url = request.form['video_url']
            for _check in app.config["WAFWTF"]:
                if _check in _url:
                    return render_template('error.html',error = "The champion need nothing!!!")
            _timestamp = request.form['timestamp']
            _time = int(time.time())
            file = parse(_url)
            f_name =  hashlib.md5(upload_secret_key+str(_time)).hexdigest()+".mp4"
            _tmp = hashlib.md5(upload_secret_key+str(_time)).hexdigest() + ".gif"
            _GIF_name = os.path.join(app.config['UPLOAD_FOLDER'], _tmp)
            time.sleep(0.5)
            if (len(_title) < 0x1337) & (len(_description) < 0x1337):
                if _timestamp:
                    if csrf_handmade(_timestamp):
                        if os.path.isfile(app.config['UPLOAD_FOLDER']+"/"+f_name):
                            return render_template('error.html', error = 'file exist homie, try another file')
                        else:
                            _f = open(os.path.join(app.config['UPLOAD_FOLDER'], f_name),"w+")
                            _f.write(file.decode('hex'))
                            _f.close()
                            _cmd = "ffmpeg -i "+str(os.path.join(app.config['UPLOAD_FOLDER'], f_name))+" "+str(_GIF_name)
                            os.system(_cmd)
                            __title, __description = racingboiz101(_title,_description)
                            return render_template('yourgif.html', gifImage=_GIF_name, title=cgi.escape(__title),description=cgi.escape(__description))
                    else:
                        return render_template('error.html',error = 'Time is money')
                else:
                    return render_template('error.html',error = "Something go wrong homie !")
            else:
                return render_template('error.html',error = "Something go wrong homie !")
        else:
            return render_template('error.html', error = 'Something go wrong homie !')
    else:
        return render_template('error.html',error = 'Unauthorized Access')

@app.route('/addUpdateLike',methods=['POST'])
def addUpdateLike():
    try:
        if session.get('user'):
            _admin_blogID = 1
            _blogId = request.form['blog']
            _like = request.form['like']
            for _check in app.config["WAFWTF"]:
                if _check in _like:
                    return render_template('error.html',error = "Something go wrong homie !")
            if _like in app.config["STRING_ONLY"]:
                return render_template('error.html',error = "Something go wrong homie !")
            if len(_like) > 3:
                return render_template('error.html',error = "Something go wrong homie !")
            _bonus_for_admin = int(float(_like))
            _user = session.get('user')
            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.callproc('sp_AddUpdateLikes',(_admin_blogID,_user,_bonus_for_admin))
            time.sleep(0.5)
            cursor.callproc('sp_AddUpdateLikes',(_blogId,_user,_like))
            data = cursor.fetchall()
            if len(data) is 0:
                conn.commit()
                cursor.close()
                conn.close()
                conn = mysql.connect()
                cursor = conn.cursor()
                cursor.callproc('sp_getLikeStatus',(_blogId,_user))
                result = cursor.fetchall()
                return json.dumps({'status':'OK','total':result[0][0],'likeStatus':result[0][1]})
            else:
                return render_template('error.html',error = 'An error occurred!')
        else:
            return render_template('error.html',error = 'Unauthorized Access')
    except Exception as e:
        return render_template('error.html',error = "Something go wrong homie !")


@app.route('/getAllBlogs')
def getAllBlogs():
    try:
        if session.get('user'):
            _user = session.get('user')
            conn = mysql.connect()
            cursor = conn.cursor()
            cursor_get_like = conn.cursor()
            cursor.callproc('sp_GetAllBlogs')
            result = cursor.fetchall()
            blogs_dict = []
            _like_list = []
            for _blog_id in result:
                cursor_get_like.callproc('sp_getLikeStatus',(_blog_id[0],_user))
                __like = cursor_get_like.fetchall()
                for i in __like:
                    for blog in result:
                        if i[2] == blog[0]:
                            blog_dict = {
                                    'Id': blog[0],
                                    'Title': blog[1],
                                    'Description': blog[2],
                                    'FilePath': blog[3],
                                    'Like':i[0],
                                    'HasLiked':i[1]}
                            blogs_dict.append(blog_dict)
            return json.dumps(blogs_dict)
        else:
            return render_template('error.html', error = 'Unauthorized Access')
    except Exception as e:
        return render_template('error.html',error = "Something go wrong homie !")


@app.route('/addBlog',methods=['POST'])
def addBlog():

    try:
        if session.get('user'):
            _timestamp = request.form['timestamp']
            _title = request.form['inputTitle']
            _description = request.form['inputDescription']
            _user = session.get('user')
            if request.form.get('filePath') is None:
                _filePath = ''
            else:
                _filePath = request.form.get('filePath')
            if request.form.get('private') is None:
                _private = 0
            else:
                _private = 1
            if request.form.get('done') is None:
                _done = 0
            else:
                _done = 1
            for _wafwtf in app.config["WAFWTF"]:
                if _wafwtf in _filePath:
                    return render_template('error.html',error = 'The champion need nothing!!!')            
            conn = mysql.connect()
            cursor = conn.cursor()
            if _timestamp:
                if csrf_handmade(_timestamp):
                    cursor.callproc('sp_addBlog',(_title,_description,_user,_filePath,_private,_done))
                    data = cursor.fetchall()
                    if len(data) is 0:
                        conn.commit()
                        return redirect('/showDashboard')
                else:
                    return render_template('error.html',error = 'Time is money')
            else:
                return render_template('error.html',error = "Something go wrong homie !")      

        else:
            return render_template('error.html',error = 'Unauthorized Access')
    except Exception as e:
        return render_template('error.html',error = "Something go wrong homie !")


@app.route('/validateLogin',methods=['POST'])
def validateLogin():
    try:
        _username = request.form['inputEmail']
        _password = request.form['inputPassword']
        con = mysql.connect()
        cursor = con.cursor()
        cursor.callproc('sp_validateLogin',(_username,))
        data = cursor.fetchall()

        if len(data) > 0:
            if check_password_hash(str(data[0][3]),_password):
                session['user'] = data[0][0]
                return redirect('/showDashboard')
            else:
                return render_template('error.html',error = 'Wrong Email address or Password or You are not a goldmember. `Matane`!!')
        else:
            return render_template('error.html',error = 'Wrong Email address or Password or You are not a goldmember. `Matane`!!')
    except Exception as e:
        return render_template('error.html',error = "Something go wrong homie !")


@app.route('/signUp',methods=['POST','GET'])
def signUp():
    try:
        _name = request.form['inputName']
        _email = request.form['inputEmail']
        _password = request.form['inputPassword']
        if _name and _email and _password:
            conn = mysql.connect()
            cursor = conn.cursor()       
            _hashed_password = generate_password_hash(_password)
            cursor.callproc('sp_createUser',(_name,_email,_hashed_password))
            data = cursor.fetchall()
            conn.commit()
            conn2 = mysql.connect()
            cursor2 = conn2.cursor()
            cursor2.callproc('sp_validate_golden_member')
            conn2.commit()
            return json.dumps("Register successfully. But only `golden` member can login")
        else:
            return render_template('error.html', error='Enter the required fields homie')
    except Exception as e:
        return json.dumps({'error':str(e)})


@app.route('/GIFmaker',methods=['POST'])
def GIFmaker():
    try:
        if session.get('user'):
            _timestamp = request.form['timestamp']
            _title = request.form['inputTitle']
            _description = request.form['inputDescription']
            _user = session.get('user')
            _time = int(time.time())
            _tmp = hashlib.md5(upload_secret_key+str(_time)).hexdigest() + ".gif"
            _GIF_name = os.path.join(app.config['UPLOAD_FOLDER'], _tmp)

            if request.form.get('filePath') is None:
                _filePath = ''
            else:
                _filePath = request.form.get('filePath')

            for _wafwtf in app.config["WAFWTF"]:
                if _wafwtf in _filePath:
                    return render_template('error.html',error = 'The champion need nothing!!!')   

            _prepare = request.host_url+str(_filePath)
            _data = parse(_prepare)

            for _check in app.config["WAFWTF"]:
                if _check in _data.decode('hex'):
                    return render_template('error.html',error = "The champion need nothing!!!")

            if (len(_title) < 0x1337) & (len(_description) < 0x1337):
                if _timestamp:
                    if csrf_handmade(_timestamp):
                        _cmd = "ffmpeg -i "+str(_filePath)+" "+str(_GIF_name)
                        os.system(_cmd)
                        __title, __description = racingboiz101(_title,_description)
                        return render_template('yourgif.html', gifImage=_GIF_name, title=cgi.escape(__title),description=cgi.escape(__description))
                    else:
                        return render_template('error.html',error = 'Time is money')   
                else:
                    return render_template('error.html',error = "Something go wrong homie !")
            else:
                return render_template('error.html',error = "Something go wrong homie !")       

        else:
            return render_template('error.html',error = 'Unauthorized Access')
    except Exception as e:
        return render_template('error.html',error = "Something go wrong homie !")


# #This is previous one aka. HelloVietNam v1 (at SVATTT2019 final). Uncomment this if u wanna try.
# #save the best for the last homie
# @app.route('/getflag',methods=['POST','GET'])
# def getflag():
#     try:
#         if session.get('user'):
#             _admin_blogID = 1
#             _blogId = request.form['blogID']
#             _timestamp = request.form['timestamp']
#             if _blogId in app.config["WAFWTF"]:
#                 return render_template('error.html',error = 'The champion need nothing!!!')
#             if str(_blogId) == str(_admin_blogID):
#                 return render_template('error.html',error = 'The champion need nothing!!!')
#             _user = session.get('user')
#             conn = mysql.connect()
#             cursor_admin = conn.cursor()
#             cursor_admin.callproc('sp_getLikeStatus',(_admin_blogID,_user))
#             result_admin = cursor_admin.fetchall()
#             cursor_admin.close()
#             conn.close()
#             time.sleep(0.5)
#             conn = mysql.connect()
#             cursor = conn.cursor()
#             cursor.callproc('sp_getLikeStatus',(_blogId,_user))
#             result = cursor.fetchall()
#             cursor.close()
#             conn.close()
#             if csrf_handmade(_timestamp):
#                 if result[0][0] > result_admin[0][0]:            
#                     flag = open('flag.txt', 'r+')
#                     _flag = flag.read()
#                     flag.close()
#                     return render_template('getflag.html',flag = _flag)
#                 return json.dumps("`Matane` in SVATTT2020 homie")
#             else:
#                 return json.dumps("`Matane` in SVATTT2020 homie")
#         else:
#             return render_template('error.html',error = 'Unauthorized Access')
#     except Exception as e:
#         return render_template('error.html',error = "Something go wrong homie !")

if __name__ == "__main__":
    app.run(host='0.0.0.0',port='31337')
