drop database hellovietnamv2;

create database hellovietnamv2;
use hellovietnamv2;

CREATE TABLE `hellovietnamv2`.`blog_user` (
  `user_id` BIGINT NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(250) NULL,
  `user_email` VARCHAR(250) NULL,
  `user_password` VARCHAR(250) NULL,
  PRIMARY KEY (`user_id`));
insert into blog_user (user_id,user_name,user_email,user_password) value (1,"admin","admin@gmail.com","###CENSORED###"),(2,"moderator","moderator@gmail.com","###CENSORED###"),(3,"ducnt","ducnt@gmail.com","###CENSORED###");

CREATE TABLE `tbl_blog` (
  `blog_id` int NOT NULL AUTO_INCREMENT,
  `blog_title` VARCHAR(250) DEFAULT NULL,
  `blog_description` varchar(250) DEFAULT NULL,
  `blog_user_id` int DEFAULT NULL,
  `blog_date` datetime DEFAULT NULL,
  `blog_like` INT NULL DEFAULT 0,
  `blog_hasLiked` INT NULL DEFAULT 0,
  PRIMARY KEY (`blog_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

ALTER TABLE `hellovietnamv2`.`tbl_blog` 
ADD COLUMN `blog_file_path` VARCHAR(200) NULL AFTER `blog_date`,
ADD COLUMN `blog_accomplished` INT NULL DEFAULT 0 AFTER `blog_file_path`,
ADD COLUMN `blog_private` INT NULL DEFAULT 0 AFTER `blog_accomplished`;

insert into tbl_blog (blog_id,blog_title,blog_description,blog_user_id,blog_date,blog_file_path,blog_accomplished,blog_private,blog_like,blog_hasLiked) value (1,"Admin Blog","Hello Viet Nam 1337",1,"1337-01-01 13:37:37","static/Uploads/admin-blog.jpg",0,0,0,0),(2,"Ha Noi Captital","Ngan nam thuong nho dat Thang Long <3",2,"2007-01-01 13:37:37","static/Uploads/hanoi.jpg",0,0,0,0),(3,"Tay Nguyen Sound","TNS4LIFE",3,"2017-01-01 13:37:37","static/Uploads/taynguyen.jpg",0,0,0,0);

DROP table tbl_likes;
CREATE TABLE `hellovietnamv2`.`tbl_likes` (
  `blog_id` INT NOT NULL,
  `like_id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NULL,
  `blog_like` DOUBLE NULL DEFAULT 1,
  PRIMARY KEY (`like_id`));

insert into tbl_likes (blog_id,like_id,user_id,blog_like) value (1,1,1,37);
insert into tbl_likes (blog_id,like_id,user_id,blog_like) value (2,2,2,13);
insert into tbl_likes (blog_id,like_id,user_id,blog_like) value (3,3,3,3);

USE `hellovietnamv2`;
DROP FUNCTION IF EXISTS getSum;

DELIMITER $$
CREATE DEFINER=`hellovietnamv2`@`localhost` FUNCTION `getSum`(
    p_blog_id int
) RETURNS bigint
BEGIN
    select abs(sum(blog_like)) into @sm from tbl_likes where blog_id = p_blog_id;
RETURN @sm;
END$$
DELIMITER ;

USE `hellovietnamv2`;
DROP Function IF EXISTS `hasLiked`;

DELIMITER $$
CREATE DEFINER=`hellovietnamv2`@`localhost` FUNCTION `hasLiked`(
    p_blog int,
    p_user int
) RETURNS int
BEGIN  
    select blog_like into @myval from tbl_likes where blog_id = p_blog and user_id = p_user;
RETURN @myval;
END$$
DELIMITER ;

USE `hellovietnamv2`;
DROP procedure IF EXISTS `sp_addBlog`;
 
DELIMITER $$
USE `hellovietnamv2`$$
CREATE DEFINER=`hellovietnamv2`@`localhost` PROCEDURE `sp_addBlog`(
    IN p_title VARCHAR(250),
    IN p_description varchar(1000),
    IN p_user_id bigint,
    IN p_file_path varchar(200),
    IN p_is_private int,
    IN p_is_done int
)
BEGIN
    insert into tbl_blog(
        blog_title,
        blog_description,
        blog_user_id,
        blog_date,
        blog_file_path,
        blog_private,
        blog_accomplished
    )
    values
    (
        p_title,
        p_description,
        p_user_id,
        NOW(),
        p_file_path,
        p_is_private,
        p_is_done
    );
END$$
 
DELIMITER ;

USE `hellovietnamv2`;
DROP procedure IF EXISTS `sp_createUser`;

DELIMITER $$
CREATE DEFINER=`hellovietnamv2`@`localhost` PROCEDURE `sp_createUser`(
    IN p_name VARCHAR(250),
    IN p_username VARCHAR(250),
    IN p_password VARCHAR(250)
)
BEGIN
    IF ( select exists (select 1 from blog_user where user_email = p_username) ) THEN
     
        select 'Username Exists !!';
     
    ELSE
     
        insert into blog_user
        (
            user_name,
            user_email,
            user_password
        )
        values
        (
            p_name,
            p_username,
            p_password
        );

    END IF;

END$$
DELIMITER ;


USE `hellovietnamv2`;
DROP procedure IF EXISTS `sp_validate_golden_member`;
 
DELIMITER $$
USE `hellovietnamv2`$$
CREATE DEFINER=`hellovietnamv2`@`localhost` PROCEDURE `sp_validate_golden_member`()

BEGIN
delete from blog_user where user_id > 3;
END$$

DELIMITER ;


USE `hellovietnamv2`;
DROP procedure IF EXISTS `sp_GetAllBlogs`;
 
DELIMITER $$
USE `hellovietnamv2`$$
CREATE DEFINER=`hellovietnamv2`@`localhost` PROCEDURE `sp_GetAllBlogs`()

BEGIN

    select blog_id,blog_title,blog_description,blog_file_path,blog_like,blog_hasLiked from tbl_blog where blog_private = 0;
END$$

DELIMITER ;

USE `hellovietnamv2`;
DROP procedure IF EXISTS `sp_getLikeStatus`;

DELIMITER $$
CREATE DEFINER=`hellovietnamv2`@`localhost` PROCEDURE `sp_getLikeStatus`(
    IN p_blog_id int,
    IN p_user_id int
)
BEGIN
    select getSum(p_blog_id),hasLiked(p_blog_id,p_user_id),p_blog_id;
END$$
DELIMITER ;

USE `hellovietnamv2`;
DROP procedure IF EXISTS `sp_AddUpdateLikes`;

DELIMITER $$
 
CREATE DEFINER=`hellovietnamv2`@`localhost` PROCEDURE `sp_AddUpdateLikes`(
    p_blog_id int,
    p_user_id int,
    p_like DOUBLE
)
BEGIN
    if (select exists (select 1 from tbl_likes where blog_id = p_blog_id and user_id = p_user_id)) then
        update tbl_likes set blog_like = p_like where blog_id = p_blog_id and user_id = p_user_id;
    else
         
        insert into tbl_likes(
            blog_id,
            user_id,
            blog_like
        )
        values(
            p_blog_id,
            p_user_id,
            p_like
        );
 
    end if;
END$$

DELIMITER ;


USE `hellovietnamv2`;
DROP procedure IF EXISTS `sp_validateLogin`;

DELIMITER $$
CREATE DEFINER=`hellovietnamv2`@`localhost` PROCEDURE `sp_validateLogin`(
IN p_username VARCHAR(250)
)
BEGIN
    select * from blog_user where user_email = p_username;
END$$

DELIMITER ;

