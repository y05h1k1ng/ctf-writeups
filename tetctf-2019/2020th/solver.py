from ptrlib import *
from mt import MT19937
import random

values = []
for i in range(0, 624, 2):
    sock = Socket('207.148.119.58', 6666)
    sock.sendline(str(i))
    sock.sendline(str(i+1))
    for j in range(2019):
        r = sock.recvline().strip()
        if r != b'Nope!':
            values.append(int(r))

print('[+] collection completed')
mt = MT19937()
predict_rand = mt.go(values)

for i in range(624, 2017):
    predict_rand.getrandbits(32)

sock = Socket('207.148.119.58', 6666)
sock.sendline('2017')
sock.sendline('2018')
for i in range(2017):
    sock.recvline()

print('[+] testing...')
a_2017 = int(sock.recvline())
a_2018 = int(sock.recvline())
print('2017 :', a_2017 == predict_rand.getrandbits(32))
print('2018 :', a_2018 == predict_rand.getrandbits(32))

prediction = predict_rand.getrandbits(32)
print('[+] prediction :', prediction)
sock.sendline(str(prediction))
print(sock.recvall())
