<?php
require_once('dbconnect.php');
require_once('auth.php');
require_once('lib.php');
require_once('_parse.php');
require_once('curl.php');

ini_set("session.cookie_httponly", 1);
ob_start();
session_start();

if ($_auth === false) {
    header("Location: index.php?page=login.php");
    exit;
}

$watch = $_GET['v'];

switch ($watch) {
    case "video2":
        header("Location: index.php?page=video2.php");
        break;
    case "video3":
        header("Location: index.php?page=video3.php");
        break;
    case "video4":
        header("Location: index.php?page=video4.php");
        break;
    case "video5":
        header("Location: index.php?page=video5.php");
        break;
    case "video6":
        header("Location: index.php?page=video6.php");
        break;
    case "video7":
        header("Location: index.php?page=video7.php");
        break;
    default:
        continue;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>MeePwnTube</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style type="text/css">
        .icon-action p {
            display: inline;
            margin: 0 20px;
        }

        .hidden-overflow {
            white-space: nowrap;
            overflow: hidden !important;
            text-overflow: ellipsis;
        }
        .card {
          box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
          max-width: 300px;
          margin: auto;
          text-align: center;
          font-family: arial;
        }

        .title {
          color: grey;
          font-size: 18px;
        }

        button {
          border: none;
          outline: 0;
          display: inline-block;
          padding: 8px;
          color: white;
          background-color: #000;
          text-align: center;
          cursor: pointer;
          width: 100%;
          font-size: 18px;
          opacity: 0.8;
        }


    </style>
    <link rel="shortcut icon" href="images/meepwntube.png">
    <html lang="en">

    <style>
    form {
        border: 3px solid #f1f1f1;
    }

    input[type=text], input[type=password] {
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        box-sizing: border-box;
    }

    button {
        background-color: #4CAF50;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        cursor: pointer;
        width: 100%;
    }

    .cancelbtn {
        width: auto;
        padding: 10px 18px;
        background-color: #f44336;
    }

    .imgcontainer {
        text-align: center;
        margin: 24px 0 12px 0;
    }

    img.avatar {
        width: 40%;
        border-radius: 50%;
    }

    .container {
        padding: 16px;
    }

    span.keypass {
        float: right;
        padding-top: 16px;
    }
    @media screen and (max-width: 300px) {
        span.keypass {
           display: block;
           float: none;
        }
        .cancelbtn {
           width: 100%;
        }
    }
    </style>

    <style>
        body {
        background: url("images/background.jpg") no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        }
    </style>

</head>

<body>
    <header class="row" style="margin: 10px 0; box-shadow: 0 4px 2px -2px #f2f2f2;">
        <div class="container">
            
            <div class="row">
                <div class="col-md-2" style="margin: 10px 0;">
                    <a href = "index.php"><img src="images/meepwntube.png" width="250px" height="150px" /></a>
                </div>
                <div class="col-md-10">
                    <div class="row">
                    <div class="col-md-5 col-md-offset-6 icon-action">
                    <div class="row" style="text-align: right; margin: 15px 0;">
                        <p>
                            <a href ="#"><i class="fa fa-video-camera fa-lg" aria-hidden="true"></i></a>
                        </p>
                        <p>
                            <a href ="#"><i class="fa fa-calendar fa-lg" aria-hidden="true"></i></a>
                        </p>
                        <p>
                            <a href ="#"><i class="fa fa-comment-o  fa-lg" aria-hidden="true"></i></a>
                        </p>
                        <p>
                            <a href ="#"><i class="fa fa-bell fa-lg" aria-hidden="true"></i></a>
                        </p>
                        <p>
                        </p>
                    </div>

                </div>
                
                <div class="col-md-1 icon-action">
                        <div id="profile">
                            <div class="icon" id="profile-icon" style="cursor: pointer">
                                <?php echo $_parse_current_avatar;?>
                            </div>

                            <div class="list-group" style="position: absolute; width: 300%; left: -250%; margin-top: 10px; z-index: 10; display: none; box-shadow: 0 1px 10px rgba(0,0,0,.1); border-width: 0;"
                                id="profile-detail">
                                <div class="list-group-item" style="background-color: #e9e9e9;">
                                    <div class="row" >
                                        <div class="col-md-2" style="text-align: center;">
                                            <?php echo $_thumbnail;?>
                                        </div>
                                        <div class="col-md-9">
                                            <div style="padding: 5px 0">
                                                <center>
                                                    <b><p style='font-size:15px; color:#0099cc'>Hello <?php echo $username ;?></p></b>
                                                    <b><p style='font-size:12px; color:#0099cc'>Having a nice day</p></b>
                                                </center>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <a href="account.php" class="list-group-item" style="display: block;">
                                        <div class="row" >
                                                <div class="col-md-2" style="text-align: center;">
                                                        <i class="fa fa-address-book fa-lg" aria-hidden="true"></i>
                                                </div>
                                                <div class="col-md-9">
                                                           My account
                                                </div>
                                            </div>
                                </a>
                                <a href="#under_construction" class="list-group-item" style="display: block;">
                                    <div class="row" >
                                        <div class="col-md-2" style="text-align: center;">
                                                <i class="fa fa-cog fa-lg" aria-hidden="true"></i>
                                        </div>
                                        <div class="col-md-9">
                                                    Creator Studio
                                        </div>
                                    </div>
                                </a>
                                <a href="logout.php" class="list-group-item" style="display: block;">
                                    <div class="row" >
                                        <div class="col-md-2" style="text-align: center;">
                                            <i class="fa fa-sign-out fa-lg" aria-hidden="true"></i>
                                        </div>
                                        <div class="col-md-9">
                                            Logout
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                </div>
                </div>

                <div align="center" class="row">
                    <b><p style='font-size:50px; color:#ff5050'>Music for Everyone</p></b>
                </div>
            </div>

            <script type="text/javascript">
                $(function () {
                    $("#profile-icon").click(function () {
                        $("#profile-detail").toggle();
                    });

                    $(document).mouseup(function(e) 
                    {
                        var container = $("#profile-detail");
                        if (!container.is(e.target) && container.has(e.target).length === 0){
                            container.hide();
                        }
                    });
                });
            </script>
            </div>
        </div>

    </header>


            <div class="container">
        <br/>

        <center>
        <form action="report.php" id="usrform" method="POST">
        </form>
        <textarea name="url" form="usrform" rows="5" cols="150" placeholder="Video URL..."></textarea><br><br>
        <input class='button' type="submit" form="usrform" value="report">

        <?php

        if(isset($_POST['url']) && !empty($_POST['url'])){
            if(strlen($_POST['url'])>300)
            {
                die('<br><br>Something go wrong homie:</br></br>');
            }
                $filename = gen_report_path($username).uniqid().'.txt';
                file_put_contents($filename, $_POST['url']);
                echo '<article class="row"> <center> <div class="card"> <h2 style="text-align:center"><p style="font-size:30px; color:#cc6699"><b>Video has been reported successfully</b></p></h2><b><p style="font-size:20px; color:green">We will check it every 37.1337s</p></b><b><p style="font-size:20px; color:green">Your report locate <a href="'.$filename.'">HERE</a></p></b> </div> </center> </article>';
            }
        ?>
        </center>
        </html>

        <footer class="row">
        </footer>
        </div>
            
            </div>
        </div>

    </header>
        

</body>

</html>

