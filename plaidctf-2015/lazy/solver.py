from sage.all import *
from binascii import unhexlify

with open('./pubkey.txt') as f:
    pk = eval(f.read())
with open('./ciphertext.txt') as f:
    c = eval(f.read())

def create_matrix(c, pk):
    n = len(pk)
    i = matrix.identity(n)*2
    last_col = [-1]*n
    first_row = []
    for p in pk:
        first_row.append(int(long(p)))
    first_row.append(-c)

    m = matrix(ZZ, 1, n+1, first_row)
    m = 1000 * m
    bottom = i.augment(matrix(ZZ, n, 1, last_col))
    m = m.stack(bottom)
    return m

def find_short_vector(matrix):
    for col in matrix.columns():
        if col[0] != 0:
            continue
        if is_short_vector(col):
            return col

def is_short_vector(vector):
    for v in vector:
        if v != 1 and v != -1 and v != 0:
            return False
    return True

m = create_matrix(c, pk)
lllm = m.transpose().LLL().transpose()
short_vector = find_short_vector(lllm)
solution_vector = []
for v in short_vector:
    if v == 1:
        solution_vector.append(1)
    elif v == -1:
        solution_vector.append(0)
a = "".join(map(str, solution_vector))
b = "1" + a[1:]
print(unhexlify(hex(int(a, 2))[2:-1]))
print(unhexlify(hex(int(b, 2))[2:-1]))
